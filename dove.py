import irc.bot
import os
import sys
import getopt
from lepl.apps.rfc3696 import HttpUrl 
from irclib import nm_to_n

CONF = {"host": "irc.epitech.net", "port": 6667, "nick": "dove"}
PRIV_COM = {"!showcurrent"}

class Dove(irc.bot.SingleServerIRCBot):
    def __init__(self, conf):
        irc.bot.SingleServerIRCBot.__init__(self, [(conf["host"],
                conf["port"])], conf["nick"], 60)
        self.channel = "#test-dove"
        self.nick = "dove"
        self.validatorUrl = HttpUrl()
        self.listUrl = []

    def on_welcome(self, serv, ev):
        print("Dove is connected to %s" % ev.source)
        serv.join("#test-dove")

    def on_pubmsg(self, serv, ev):
        cl = ev.arguments[0].lower().split()
        valid = HttpUrl()
        url = ''
        lhash = []
        for it in cl:
            if self.validatorUrl(it) == True:
                url = it 
            if it[0] == '#':
                lhash.append(it)
        if len(url) > 0:
            self.listUrl.append((url, lhash)) 
            files = open("dove.bdd", "a")
            files.write(str((url, lhash)))
            files.close()
            print(self.listUrl)

    def showcurrent(self, ev, c, author):
        c.privmsg(author, str(self.listUrl))

    def on_privmsg(self, serv, ev):
        print(ev.source)
        author = nm_to_n(ev.source)
        com = ev.arguments[0].split()
        if com[0] in PRIV_COM:
            getattr(self, com[0][1:])(ev, self.connection, author)

if __name__ == '__main__':
    try:
        opt, arg = getopt.getopt(sys.argv[1:], "c:")
    except getopt.GetoptError as err:
        print(err)

    dove = Dove(CONF)
    dove.start()
